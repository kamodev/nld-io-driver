// All the needed linux includes
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/slab.h>

// Low Level include
#include <asm/io.h>

// Setup the module
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jeremiah Bagula (N0KMO)");
MODULE_DESCRIPTION("Sample code of writing drivers for RASPI");
MODULE_VERSION("1.0.0");

// Constants for the driver
#define MAX_USR_SIZE 1024
#define BCM2837_GPIO_ADDR 0x3F200000

// Global variables needed
static struct proc_dir_entry *driver_proc = NULL;
static char buffer[MAX_USR_SIZE + 1] = {0};
static unsigned int *pin_io_regs = NULL;

// GPIO pin io for on state
static void pin_on(unsigned int pin)
{
    // Setup for accessing the register for the IO to pysical PINs
	unsigned int idx = pin / 10;
	unsigned int bit_pos = pin % 10;
	unsigned int* pin_sel = pin_io_regs + idx;
	unsigned int* pin_on_reg = (unsigned int*)((char*)pin_io_regs + 0x1c);

	/// Bit twiddling to activate the pin registers
	*pin_sel &= ~(7 << (bit_pos * 3));
	*pin_sel |= (1 << (bit_pos * 3));
	*pin_on_reg |= (1 << pin);
    
	// printk("Turned pin %d on", pin);

	return;
}

// GPIO pin io for OFF state
static void pin_off(unsigned int pin)
{
	// Setup the register
	unsigned int *off_register = (unsigned int*)((char*)pin_io_regs + 0x28);
	
	// Bit twiddling to turn off
	*off_register |= (1 << pin);
	
	// printk("Turned pin %d off", pin);

	return;
}

ssize_t driver_read(struct file *file, char __user *usr, size_t size, loff_t *off)
{
	return 0;
}

ssize_t driver_write(struct file *file, const char __user *usr, size_t size, loff_t *off)
{
	unsigned int pin = UINT_MAX;
	unsigned int value = UINT_MAX;

	// Set the memory size needed
	memset(buffer, 0x0, sizeof(buffer));

	if (size > MAX_USR_SIZE)
	{
		size = MAX_USR_SIZE;
	}

	if (copy_from_user(buffer, usr, size))
    {
	    return 0;
    }

	printk("Data buffer: %s\n", buffer);

	if (sscanf(buffer, "%d,%d", &pin, &value) != 2)
	{
		printk("Incorrect Pin and Data format\n");
		return size;
	}

	if (pin > 21 || pin < 0)
	{
		printk("Invalid pin selected please use a valid pin\n");
		return size;
	}

	if (value != 0 && value != 1)
	{
		printk("Pin value of either on=1 or off=0 is incorrect\n");
		return size;
	}

	printk("Using pin %d with value of %d\n", pin, value);

	if (value > 0)
	{
		pin_on(pin);
	} 
    else
	{
		pin_off(pin);
	}

	return size;
}

static const struct proc_ops proc_file_ops = 
{
	.proc_read = driver_read,
	.proc_write = driver_write,
};

// Install the driver
static int __init driver_init(void)
{
	// Map the pin registers
	pin_io_regs = (int*)ioremap(BCM2837_GPIO_ADDR, PAGE_SIZE);
	
	// Check the mapping
    if (pin_io_regs == NULL)
	{
		printk("Failed to map GPIO memory to driver\n");
		return -1;
	}
	
	// create an entry in the proc-fs
	driver_proc = proc_create("nld-io-driver", 0666, NULL, &proc_file_ops);
	if (driver_proc == NULL)
	{
		return -1;
	}

	return 0;
}

// Remove the driver
static void __exit driver_exit(void)
{
	// Unmap the registers
	iounmap(pin_io_regs);

	// Remove the PROC entry
	proc_remove(driver_proc);

	return;
}

module_init(driver_init);
module_exit(driver_exit);
