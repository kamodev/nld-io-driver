// All the Linux included headers
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main()
{
    // Open pointer to the Proc file
	int proc_file = open("/proc/nld-io-driver", O_RDWR);

    // Loop forever
	while(1)
	{
        // Write to the Driver to turn on pin
		write(proc_file, "20,1", 4);
        printf("Turning on pin 20 with value of 1\n");

        // Sleeping
		usleep(50000);

        // Write to the Driver to turn off pin
		write(proc_file, "20,0", 4);
        printf("Turning off pin 20 with value of 0\n");
		
        // Sleeping
        usleep(100000);
	}
}